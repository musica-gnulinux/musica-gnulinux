# musica-gnulinux - Musicisti GNU+Linux

Questo progetto è nato per offrire ai musicisti una guida in italiano e facile 
da capire su come registrare, produrre, mixare, masterizzare e suonare musica 
live su sistemi GNU+Linux.

Esso si concentra sull'utilizzo di software libero, con un 
approccio low-budget ispirato da ideali di sostenibilità, contro la 
cultura dello scarto - si può fare buona musica anche su vecchi computer!

## Come contribuire

Tutto il testo in questo repository fa uso di Markdown. 

Per contribuire, ci sono tre opzioni: 
+ Iscriversi su GitLab e usare il suo editor di testo
+ Inviare una pull request su GitLab
+ Inviare il file Markdown a `musicalinux presso tuxfamily.org` 
assieme ad una dichiarazione che afferma che il file è sotto licenza Creative 
Commons Attribution 4.0 International

Anche i feedback e le richieste sono importanti contributi alla crescita del 
progetto, sentitevi liberi di contattarci usando l'issue tracker o una email a 
`musicalinux presso tuxfamily.org`

## Ringraziamenti

Oltre agli autori, un grazie enorme al team di TuxFamily ed a tutti i membri 
del gruppo @linuxmusica su Telegram.

## Licenza

Tutti i file presenti in questo repository sono rilasciati sotto 
licenza Creative Commons Attribution 4.0 International.
